"""This is the main module, which starts the application."""

import gi
import os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, Gio, GLib

from modules import IdeApplicationWindow
from modules import IdeThreadManager
from modules.IdeSettings import IdeSettings


class IdeApplication(Gtk.Application):
    """Docstrings for IdeApplication."""

    def __init__(self, *args, **kwargs):
        """Docstrings for __init__."""
        # TODO use the Gio handles (OPEN and COMMMAND_LINE)
        super().__init__(*args, application_id="com.raggesilver.PyIDE",
                         flags=Gio.ApplicationFlags.FLAGS_NONE,
                         **kwargs)

        self.window = None
        self.thread_manager = IdeThreadManager.IdeThreadManager

        GLib.set_application_name("PyIDE")

    def do_startup(self):
        """do_startup docstrings."""
        Gtk.Application.do_startup(self)

        action = Gio.SimpleAction.new("quit", None)
        action.connect("activate", self.on_quit)
        self.add_action(action)

        action = Gio.SimpleAction.new("about", None)
        action.connect("activate", self.on_about)
        self.add_action(action)

        # self.add_action(action)

        builder = Gtk.Builder.new_from_file(os.path.join(
            IdeSettings.root, "layout", "MainWindowShellMenu.ui"))
        self.set_app_menu(builder.get_object("app-menu"))

    def do_activate(self, *args):
        """do_activate docstrings."""
        print('ACTIVATE', args)
        # Preventing the creation of multiple windows
        if not self.window:
            # TODO maybe make this naming shorter?
            self.window = IdeApplicationWindow.IdeApplicationWindow(
                application=self, title='PyIde')

        self.window.present()
        self.window.set_role("PyIDE")

    def _on_preferences_quit(self, *args):
        self.preferences_window.hide()

    def on_save(self, *args):
        """on_save docstrings."""
        print('SAVE')

    def on_about(self, action, param):
        """on_about docstrings."""
        # TODO create a decent about window
        builder = Gtk.Builder.new_from_file(os.path.join(
            IdeSettings.root, "layout", "AboutWindow.ui"))
        about_dialog = builder.get_object('about_dialog')
        about_dialog.set_transient_for(self.window)
        about_dialog.set_modal(True)
        about_dialog.present()

    def on_quit(self, action, param):
        """on_quit docstrings."""
        # TODO pass the signal to IdeApplicationWindow so it can make
        # the necessary checks (all files saved and stuff)
        self.quit()


if __name__ == "__main__":

    screen = Gdk.Screen.get_default()
    provider = Gtk.CssProvider()
    provider.load_from_path(os.path.join(
        IdeSettings.root,
        "layout",
        "custom.css"
    ))

    Gtk.StyleContext.add_provider_for_screen(
        screen, provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

    a = IdeApplication()

    try:
        a.run()
    except KeyboardInterrupt:
        pass
