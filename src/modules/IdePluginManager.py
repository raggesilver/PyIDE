"""IdePluginManager module."""
import os
import pkg_resources
import json

from importlib import util
from gi.repository import GObject
from pkg_resources import DistributionNotFound, VersionConflict

from modules.IdeSettings import IdeSettings


class IdePluginManager(GObject.GObject):
    """Docstrings for IdePluginManager."""

    def __init__(self, *args, **kwargs):
        """Docstrings for __init__."""
        GObject.GObject.__init__(self)

        self.settings = IdeSettings.data['ide']
        self.application_window = kwargs['application_window']

        self.updatesAvailable = {}
        self.plugins = {}

        self.load_plugins()

    # Load plugins
    def load_plugins(self):
        """Docstrings for load_plugins."""
        pluginsPath = os.path.join(IdeSettings.root, "plugins")

        for plugDir in os.listdir(pluginsPath):

            pluginPath = os.path.join(pluginsPath, plugDir)
            pluginName = os.path.basename(pluginPath)
            pluginData = None

            # If the plugin is disbled on settings
            if ('disabled_plugins' in self.settings and
                    pluginName in self.settings['disabled_plugins']):
                # Skip it's loading
                print("%s disabled, skipping..." % pluginName)
                continue

            # If all the required files are OK
            if not self.check_invalid_plugin_files(pluginPath):
                try:
                    with open(os.path.join(pluginPath, 'plugin.json')) as f:
                        pluginData = json.load(f)
                except Exception:
                    print("Couldnt read %s json" % pluginPath)
                    continue

                dependencies = (pluginData['dependencies']
                                if 'dependencies' in pluginData else [])

                # Check if there are dependency errors
                if not self.check_dependency_errors(dependencies):

                    # Importing the plugin module via importlib.util
                    spec = util.spec_from_file_location(
                        pluginName + "." + "plugin",  # Package naming
                        os.path.join(pluginPath, "plugin.py")  # Path
                    )

                    foo = util.module_from_spec(spec)
                    spec.loader.exec_module(foo)

                    # Instantiating the plugin class to the plugins object
                    self.plugins[pluginName] = foo.Plugin(
                        application_window=self.application_window,
                        settings=IdeSettings)
                    if hasattr(self.plugins[pluginName], 'commands'):
                        self.application_window.command_palette\
                            .append_commands(self.plugins[pluginName].commands)
                    # TODO decide wether the plugin should be activated here
                    # or in its own file
                    self.plugins[pluginName].do_activate()

                    print("Plugin %s loaded." % pluginName)

                else:
                    print("Skipped %s due to dependency erros." % pluginName)

            else:
                print("Skipped %s." % pluginName)

    # Check if the path has the necessary files for a plugin to be loaded
    def check_invalid_plugin_files(self, path):
        """Docstrings for check_invalid_plugin_files."""
        required = ['plugin.py', 'plugin.json']

        for i in range(len(required)):
            if not os.path.isfile(os.path.join(path, required[i])):
                print("Invalid plugin '%s' is missing." % required[i])
                return True

        return False

    # Used to check if the plugin's dependencies are installed
    def check_dependency_errors(self, dependencies):
        """Docstrings for check_dependency_errors."""
        # here, if a dependency is not met, a DistributionNotFound or
        # VersionConflict exception is thrown.
        try:

            pkg_resources.require(dependencies)
            return False  # No errors, everything OK

        except (DistributionNotFound, VersionConflict) as error:

            # TODO try installing the missing dependency
            # TODO decide wether the dependency will be installed by the
            # manager or by the plugin itself (letting the manager do it)
            # might be a better idea

            print(error)

            return error
