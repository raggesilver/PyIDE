"""IdeCommandPallete.py."""
import os
import re
import cairo

from modules.IdeSettings import IdeSettings
from gi.repository import Gtk, Gdk, GObject


class IdeCommandPalette(GObject.GObject):
    """docstring for IdeCommandPalette."""

    def __init__(self, *args, **kwargs):
        """."""
        GObject.GObject.__init__(self)
        self.application_window = kwargs['application_window']

        builder = Gtk.Builder.new_from_file(os.path.join(
            IdeSettings.root, "layout", "IdeCommandPalette.ui"))
        self.layout = builder.get_object('layout')
        self.layout.set_transient_for(self.application_window)
        self.layout.connect('delete-event', self._hide)
        self.completion_list_box = builder.get_object('completion_list_box')
        self.no_results_lbl = builder.get_object('no_results_lbl')
        self.completion_list_box.set_placeholder(self.no_results_lbl)
        self.completion_list_box.connect('move-cursor', self._on_move_cursor)
        self.completion_list_box.connect('row-selected', self._on_row_selected)

        self.row_lock = False

        screen = self.application_window.get_screen()
        visual = screen.get_rgba_visual()
        if visual and screen.is_composited():
            self.layout.set_visual(visual)
            self.layout.connect('draw', self._draw)
            self.layout.set_app_paintable(True)

        # self.box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self.entry = builder.get_object('entry')
        self.entry.connect('activate', self._activate)
        self.buffer = self.entry.get_buffer()

        self.completions = []

        self.entry.set_width_chars(60)
        self.entry.connect('changed', self._match_func)
        self.entry.connect('key-press-event', self._on_key_press)

        self._create_rows()

    def _on_move_cursor(self, lb, step, p0):
        return True

    def _show(self, *args):
        self.layout.show()
        self.entry.grab_focus()
        self.entry.set_text("")
        self._match_func()

    def _hide(self, *args):
        self.layout.hide()
        return True

    def _draw(self, widget, context):
        context.set_source_rgba(0, 0, 0, 0)
        context.set_operator(cairo.OPERATOR_SOURCE)
        context.paint()
        context.set_operator(cairo.OPERATOR_OVER)

    def _activate(self, *args):
        row = self.completion_list_box.get_selected_row()
        if row:
            self._on_row_selected(None, row)
        self.entry.grab_focus()
        return True

    # Completion model:
    #
    # {
    #   pre: "Application",
    #   text: "New Window",
    #   row: row_object,
    # }
    #
    # What is displayed in the row_object
    #   Application: New Window
    #
    # What is checked in the matching function
    #   New Window
    #
    def _match_func(self, *args):
        self.completion_list_box.show_all()
        for completion in self.completions:
            if not self._match(self.entry.get_text(), completion):
                completion['row'].hide()

        self.row_lock = True
        # Select the first row by default
        r = self.completion_list_box.get_row_at_index(0)
        # FIXME this code bellow sometimes does nothing
        self.completion_list_box.select_row(r)
        self.row_lock = False

    def _match(self, key, completion):
        if key == "":
            return False
        try:
            if (re.match(r".*{}.*".format(key),
                         completion['pre'],
                         re.IGNORECASE)):
                    return True
            elif (re.match(r".*{}.*".format(key),
                           completion['text'],
                           re.IGNORECASE)):
                    return True
        except Exception:
            return False

        return False

    def _selected(self, *args):
        print('activate')
        # self.entry.do_activate()

    def _on_row_selected(self, lb, row):
        if not self.row_lock:
            for completion in self.completions:
                if completion['row'] == row:
                    completion['fn']()
            self._hide()

    def _on_key_press(self, widget, event):
        _row = self.completion_list_box.get_selected_row()

        # If there is no row don't interfere in the event
        if not _row:
            return False

        index = (_row.get_index() or 0)
        self.row_lock = True

        if event.keyval == Gdk.KEY_Down:

            row = self.completion_list_box.get_row_at_index(index + 1)
            if row:
                self.completion_list_box.select_row(row)
            self.row_lock = False
            return True

        elif event.keyval == Gdk.KEY_Up:

            row = self.completion_list_box.get_row_at_index(index - 1)
            if row:
                self.completion_list_box.select_row(row)
            self.row_lock = False
            return True

        self.row_lock = False

    def _create_rows(self, *args):
        for completion in self.completions:
            self._create_row(completion)

    def _create_row(self, completion):
        row = Gtk.ListBoxRow()
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        box.set_margin_top(10)
        box.set_margin_bottom(10)
        box.set_margin_right(10)
        box.set_margin_left(10)
        text_lbl = Gtk.Label(completion['text'])
        pre_lbl = Gtk.Label(completion['pre'])
        pre_lbl.set_sensitive(False)
        box.pack_start(pre_lbl, False, False, 0)
        box.pack_start(text_lbl, False, False, 6)
        row.add(box)
        row.show_all()
        completion['row'] = row
        self.completion_list_box.insert(row, -1)

    def append_commands(self, commands):
        """Append a command to the command palette."""
        for command in commands:
            self.completions.append(command)
            self._create_row(command)
