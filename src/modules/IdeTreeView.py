"""IdeTreeView module."""
import os
import stat  # os.stat and stat are not the same thing

from gi.repository import GdkPixbuf
from gi.repository import Gtk, GObject, GLib

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler


class IdeTreeViewWatchdogHandler(GObject.GObject, FileSystemEventHandler):
    """Docstrings for IdeTreeViewWatchdogHandler."""

    __gsignals__ = {
        "changed": (
            GObject.SignalFlags.RUN_LAST, None, (str, bool, str, str,)
        )
    }

    def __init__(self):
        """Docstring for __init__."""
        GObject.GObject.__init__(self)

    def on_any_event(self, event):
        """Handle every event fired."""
        self.emit("changed",
                  event.event_type,
                  event.is_directory,
                  event.src_path,
                  event.dest_path if hasattr(event, 'dest_path') else None)


class IdeTreeView(Gtk.TreeView):
    """Docstrings for IdeTreeView."""

    def __init__(self, *args, **kwargs):
        """Docstring for __init__."""
        super().__init__()

        self.path = kwargs['path'] if 'path' in kwargs else '.'
        self.path = os.path.abspath(self.path)
        self.selected_path = None
        self.selected_tree_path = None
        self.icon_provider = None
        self.do_build_tree_view()

        self.set_activate_on_single_click(False)

        # Add the file watcher

        self.watcher_handler = IdeTreeViewWatchdogHandler()
        self.watcher = Observer()
        self.watcher.schedule(self.watcher_handler, self.path, recursive=True)
        self.watcher.start()  # Already creates it's own thread

        self.watcher_handler.connect("changed", self.on_watcher_changed)

    def on_watcher_changed(self, handler, event, _is_dir, src_path, dest_path):
        """Docstring for on_watcher_changed."""
        print("EVENT: %s" % event)

        if event == 'deleted':
            print('delete')

        if event == 'moved' and not _is_dir:
            print('File moved from %s to %s' % (src_path, dest_path))

        if event == 'modified' and _is_dir:
            print('Updating %s' % src_path)
            # Must call the update functions in the main thread
            GLib.idle_add(self.update_tree_folder, src_path)

    # TODO def on_file_moved(self, src_path, dst_path)
    # to achieve this I must emit a signal with these
    # information for IdeEditor to pick it up

    def update_tree_folder(self, src_path, _tree_path="",
                           _index=0, _iter=None):
        """Docstring for update_tree_folder."""
        relative_path = src_path.replace(self.path + "/", "")
        relative_path_arr = relative_path.split(os.sep)  # os separator

        model = self.get_model()

        # root file changes
        if relative_path == "" or relative_path == src_path:
            # TODO since a folder is being updated, skipping folders removal
            # is a simple and nice way to improve performance
            model.clear()
            self.do_populate_tree_view()
            return

        # Iterate in the current node
        for i in range(model.iter_n_children(_iter)):

            it = model.iter_nth_child(_iter, i)
            val = model.get_value(it, 0)
            _path = model.get_path(it)

            # If current iter is the one we are looking for
            if val == relative_path_arr[_index]:
                # and if it's the last in the path
                if (_index + 1) == len(relative_path_arr):
                    # print('Full treepath: %s' % _path)

                    # found the full path, now clean and repopulate

                    # cleaning
                    child = model.iter_children(it)
                    while child is not None:
                        tmp = model.iter_next(child)
                        self.workspace_tree_store.remove(child)
                        child = tmp

                    # repopulate
                    self.do_populate_tree_view(path=src_path, parent=it)

                    return None  # OK
                # and it's not the last in the path
                else:
                    # print('not finished')
                    return self.update_tree_folder(src_path, _tree_path=_path,
                                                   _index=(_index+1), _iter=it)

    def on_watcher_cb(self, *args):
        """Docstring for on_watcher_cb."""
        print('Watcher shut down')

    def _on_monitor_event(self, _monitor, _file, _other_file, _event_type):
        print(_event_type)

    def do_build_tree_view(self):
        """Docstring for do_build_tree_view."""
        # Gtk.TreeStore(not_sure_why, icon, label)
        self.workspace_tree_store = Gtk.TreeStore(str, GdkPixbuf.Pixbuf, str)
        self.set_model(self.workspace_tree_store)
        self.do_populate_tree_view()

        # Create the column and the rendering cells
        column = Gtk.TreeViewColumn('Project')
        text_col_cell = Gtk.CellRendererText()
        pix_col_cell = Gtk.CellRendererPixbuf()

        # Pack the cells
        column.pack_start(pix_col_cell, False)
        column.pack_start(text_col_cell, True)

        # Set the cell attributes
        column.add_attribute(text_col_cell, "text", 0)
        column.add_attribute(pix_col_cell, "pixbuf", 1)

        # This fixes the TreeView not resizing after folding content
        column.set_sizing(Gtk.TreeViewColumnSizing.AUTOSIZE)

        # Append the column to the treeview
        self.append_column(column)

        self.selected_row = self.get_selection()
        self.selected_row.connect("changed", self.on_row_selected)
        self.set_headers_visible(False)

    def on_row_selected(self, selection):
        """Docstring for on_row_selected."""
        model, row = selection.get_selected()

        if row is not None:

            self.selected_tree_path = model.get_path(row)

            fullpath = ''
            cur = row

            while cur is not None:

                fullpath = os.path.join(model[cur][0], fullpath)
                cur = model.iter_parent(cur)

            self.selected_path = os.path.abspath(fullpath)

            if not os.path.exists(self.selected_path):
                return

            item_metadata = os.stat(self.selected_path)
            # If the item is a folder
            if stat.S_ISDIR(item_metadata.st_mode):
                self.expand_row(self.selected_tree_path, False)

    def do_populate_tree_view(self, path=None, parent=None):
        """Docstring for do_populate_tree_view."""
        path = path if path else self.path

        # Listing the directory and sorting in ASC order
        _list = os.listdir(path)
        _list.sort(key=str.lower)

        # Sorting idea for the future
        # https://stackoverflow.com/a/29922970/5609203

        for item in _list:

            item_fullname = os.path.join(path, item)
            # Extract metadata from the item
            item_metadata = os.stat(item_fullname)
            # Determine if the item is a folder
            item_is_folder = stat.S_ISDIR(item_metadata.st_mode)

            # TODO create an icon plugin
            icon = None

            if self.icon_provider:
                icon = self.icon_provider.get_icon(item_fullname)

            currentIter = self.workspace_tree_store.append(
                parent, [item, icon, item_fullname])

            # If the item is a folder, call this function again
            # passing the current iter as the parent
            if item_is_folder:
                self.do_populate_tree_view(item_fullname, currentIter)

    def rebuild_tree_view(self, *args):
        """Docstrings default."""
        self.workspace_tree_store.clear()
        self.do_populate_tree_view()
