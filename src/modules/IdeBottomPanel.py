"""IdeBottomPanel module."""
import gi
import os
gi.require_versions({'Gtk': '3.0', 'Vte': '2.91'})

from gi.repository import Gtk, Vte, GObject, GLib
from modules.IdeSettings import IdeSettings


class IdeTerminal(GObject.GObject):
    """Docstrings for IdeTerminal."""

    def __init__(self, *args, **kwargs):
        """Docstrings for __init__."""
        GObject.GObject.__init__(self)

        self.application_window = kwargs['application_window']

        self.layout = Gtk.Overlay()
        self.terminal = Vte.Terminal()

        self.shell = os.environ.get('SHELL')

        self.terminal.spawn_sync(Vte.PtyFlags.DEFAULT,
                                 None,
                                 [self.shell],
                                 [],
                                 GLib.SpawnFlags.DO_NOT_REAP_CHILD,
                                 None,
                                 None,)

        self.floating_button = Gtk.Button.new_from_icon_name(
            "view-more-symbolic", Gtk.IconSize.MENU)

        self.application_window.connect('style-updated', self._set_terminal_bg)
        self._set_terminal_bg()

        self.floating_button.props.valign = Gtk.Align.START
        self.floating_button.props.halign = Gtk.Align.END
        self.floating_button.get_style_context().add_class('flat')
        self.floating_button.set_margin_top(2)
        self.floating_button.set_margin_right(2)
        self.floating_button.set_opacity(.95)

        # Building the layout
        self.layout.add(self.terminal)
        self.layout.add_overlay(self.floating_button)

        self.layout.show()

    def _set_terminal_bg(self, *args):
        self.terminal.set_color_background(
            self.application_window.get_style_context()
                .lookup_color("theme_base_color")[1]
        )


class IdeBottomPanel(GObject.GObject):
    """Docstrings for IdeBottomPanel."""

    def __init__(self, *args, **kwargs):
        """Docstrings for __init__."""
        GObject.GObject.__init__(self)

        self.application_window = kwargs['application_window']

        self.builder = Gtk.Builder.new_from_file(os.path.join(
            IdeSettings.root, "layout", "IdeBottomPanel.ui"))
        self.layout = self.builder.get_object('bottom_panel')

        self.stack = self.builder.get_object('bottom_panel_stack')

        self.terminal_radio_button =\
            self.builder.get_object('terminal_radio_button')
        self.problems_ratio_button =\
            self.builder.get_object('problems_ratio_button')
        self.output_radio_button =\
            self.builder.get_object('output_radio_button')
        self.debug_radio_button =\
            self.builder.get_object('debug_radio_button')

        self.terminal = IdeTerminal(application_window=self.application_window)
        self.output_scroll = self.builder.get_object('output_scroll')
        self.output_text_view = self.builder.get_object('output_text_view')
        self.output_text_buffer = self.builder.get_object('output_text_buffer')
        self._has_appended_output = False
        self.output_layout = self.builder.get_object('output_layout')

        self.terminal.layout.show_all()

        self.stack.add_named(self.terminal.layout, "terminal")
        self.stack.add_named(self.output_layout, "output")

        self.terminal_radio_button.connect('toggled',
                                           self._on_terminal_toggled)
        self.output_radio_button.connect('toggled', self._on_output_toggled)

    def _on_terminal_toggled(self, *args):
        """Docstrings for _on_terminal_toggled."""
        if self.terminal_radio_button.get_active():
            self.stack.set_visible_child_name("terminal")

    def _on_output_toggled(self, *args):
        """Docstrings for _on_output_toggled."""
        if self.output_radio_button.get_active():
            self.stack.set_visible_child_name("output")

    def append_to_output(self, text):
        """Append text to the output text view."""
        if type(text) == bytes:
            text = text.decode("utf-8")

        if not self._has_appended_output:
            self.output_text_buffer.set_text("")
            self._has_appended_output = True

        self.output_text_buffer.insert(
            self.output_text_buffer.get_end_iter(), text, -1)

        # TODO set this as an option
        # this makes the scrolled window scroll to bottom whenever a
        # new text is inserted
        self.output_scroll.get_vadjustment().set_value(
            self.output_scroll.get_vadjustment().get_upper())

    def show_output(self, *args):
        """Set the current bottom panel view to the output."""
        self.output_radio_button.set_active(True)
        self.layout.show()
