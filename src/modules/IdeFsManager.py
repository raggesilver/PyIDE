"""IdeFsManager module."""
# NOTE: This module will probably be deleted in the future
import os


class IdeFile:
    """Docstrings for IdeFile."""

    def __init__(self, *args, **kwargs):
        """Docstrings for __init__."""
        super().__init__()

        self.path = kwargs['path'] if 'path' in kwargs else '.'
        self.path = os.path.abspath(self.path)

    def save(self, buffer):
        """Docstrings for save."""
        try:

            with open(self.path, 'w+', encoding='utf8') as f:
                text = buffer.get_text(buffer.get_start_iter(),
                                       buffer.get_end_iter(),
                                       False)
                print(text)
                f.write(text)
                return True
        except Exception as e:
            print(repr(e))  # Printing the exception
            return False

    def read_content(self, *args):
        """Docstrings for read_content."""
        try:
            with open(self.path, 'r', encoding='utf8') as f:
                # print(f.read())
                return f.read()
        except Exception:
            # print(repr(e))  # Printing the exception
            return None
