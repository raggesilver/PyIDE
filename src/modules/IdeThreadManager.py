"""IdeThreadManager module."""
import os
import sys

from gi.repository import GObject

import threading

from IPython.core import ultratb

sys.excepthook = ultratb.FormattedTB(mode='Verbose',
                                     color_scheme='Linux',
                                     call_pdb=True,
                                     ostream=sys.__stdout__)

from colorlog import ColoredFormatter

import logging
from functools import wraps


def setup_logger():
    """Return a logger with a default ColoredFormatter."""
    formatter = ColoredFormatter(
        "(%(threadName)-9s) %(log_color)s\
        %(levelname)-8s%(reset)s %(message_log_color)s%(message)s",
        datefmt=None,
        reset=True,
        log_colors={
            'DEBUG': 'cyan',
            'INFO': 'green',
            'WARNING': 'yellow',
            'ERROR': 'red',
            'CRITICAL': 'red',
        },
        secondary_log_colors={
            'message': {
                'ERROR': 'red',
                'CRITICAL': 'red',
                'DEBUG': 'yellow'
            }
        },
        style='%'
    )

    logger = logging.getLogger(__name__)
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)

    return logger


def trace(func):
    """Tracing wrapper to log when function enter/exit happens.

    :param func: Function to wrap
    :type func: callable
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        logger.debug('Start {!r}'. format(func.__name__))
        result = func(*args, **kwargs)
        logger.debug('End {!r}'. format(func.__name__))
        return result
    return wrapper


# Create a player
logger = setup_logger()


# Based on https://gist.github.com/bossjones/e21b53c6dff04e8fdb3d
# Override GObject.GObject to always emit signals in the main thread by
# emmitting on an idle handler
class _IdleObject(GObject.GObject):
    """GObject override."""

    @trace
    def __init__(self):
        GObject.GObject.__init__(self)

    # @trace
    def emit(self, *args):
        GObject.idle_add(GObject.GObject.emit, self, *args)


# Cancellable thread that uses GObject signals to emit information to
# the GUI on the main thread
class _IdeThread(threading.Thread, _IdleObject):
    """GObject + thread class."""

    __gsignals__ = {
        "completed": (
            GObject.SignalFlags.RUN_LAST, None, (GObject.TYPE_PYOBJECT,)
        )
    }

    @trace
    def __init__(self, *args):

        threading.Thread.__init__(self)
        _IdleObject.__init__(self)
        self.cancelled = False
        self.fn = args[0]  # Function to run in thread
        self.fnArgs = (args[1] if args[1] is not None
                       else [])  # Arguments for function
        self.name = args[2]
        self.setName("%s" % self.name)

    # Python threads are not cancellable, so we must create our own
    # thread cancelling logic
    @trace
    def cancel(self):
        self.cancelled = True  # Pretty logic HUH?

    @trace
    def run(self):
        res = self.fn(*self.fnArgs)  # Call the function
        self.emit("completed", res)  # Emit completed


class _IdeThreadManager:
    """Docstrings for IdeThreadManager."""

    @trace
    def __init__(self, maxThreads=os.cpu_count()):

        self.maxThreads = maxThreads
        self.threads = {}
        self.pendingThreadArgs = []

    @trace
    def _register_thread_completed(self, thread, *args):
        print(args)

        LAST_ARG_INDEX = len(args) - 1  # AKA thread name

        del(self.threads[args[LAST_ARG_INDEX]])
        running = len(self.threads) - len(self.pendingThreadArgs)

        print("%s completed. %s running, %s pending" % (
            thread,
            running,
            len(self.pendingThreadArgs)
        ))

        if running < self.maxThreads:
            try:
                args = self.pendingThreadArgs.pop()
                print("Starting pending %s"
                      % self.threads[args[LAST_ARG_INDEX]])
                self.threads[args[LAST_ARG_INDEX]].start()

            except IndexError:
                pass

    @trace
    def make_thread(self, completeCallback, *args):
        """Append a thread to the IdeThreadManager, and execute ASAP.

        :param completeCallback: (Function) the function to run after the
        thread has finished
        :param *args: first parameter is the function that the thread will
        execute, second is the args the function will receive, last
        is the name of the thread (must be unique, can't have two
        threads with the same name at the same time)
        :returns: None
        """
        running = len(self.threads) - len(self.pendingThreadArgs)

        LAST_ARG_INDEX = len(args) - 1  # AKA thread name

        if args[LAST_ARG_INDEX] not in self.threads:
            thread = _IdeThread(*args)
            # signals run in the order connected. Connect the user completed
            # run user callback first
            # then delete the thread

            thread.connect("completed", completeCallback)
            thread.connect("completed",
                           self._register_thread_completed,
                           *args)

            self.threads[args[LAST_ARG_INDEX]] = thread

            if running < self.maxThreads:
                print("Starting %s" % thread)
                self.threads[args[LAST_ARG_INDEX]].start()
            else:
                print("Queing %s" % thread)
                self.pendingThreadArgs.append(args[LAST_ARG_INDEX])

    # Stops all threads. If block is True then actually wait for the
    # thread to finish (may block the UI)
    @trace
    def stop_all_threads(self, block=False):
        for thread in self.threads.values():

            thread.cancel()

            if block and thread.isAlive():
                thread.join()  # wait until the thread actually finishes

    @trace
    def stop_thread(self, name):
        """Blind trial to cancel a single specific thread."""
        if name in self.threads:
            thread = self.threads[name]
            print("Cancelling %s" % thread)
            thread.cancel()


IdeThreadManager = _IdeThreadManager()
