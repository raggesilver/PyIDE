"""IdeApplicationWindow module provides the window for the application."""
import os

from gi.repository import Gtk, Gdk, Gio
from modules import IdeTreeView
from modules.IdeBottomPanel import IdeBottomPanel
from modules.IdeEditor import IdeEditor
from modules.IdeSettings import IdeSettings
from modules.IdePluginManager import IdePluginManager
from modules.IdeCommandPalette import IdeCommandPalette


class Handler:
    """Docstrings default."""

    def toggle_revealer(self, revealer):
        """Docstrings default."""
        revealer.set_reveal_child(not revealer.get_reveal_child())


class IdeApplicationWindow(Gtk.ApplicationWindow):
    """Docstrings for IdeApplicationWindow."""

    def on_project_toggled(self, *args):
        """Docstrings default."""
        self.project_revealer.set_reveal_child(
            not self.project_revealer.get_reveal_child()
        )

    def __init__(self, *args, **kwargs):
        """Docstrings for __init__."""
        super().__init__(*args, **kwargs)

        self.set_default_size(800, 350)
        self.accel_group = Gtk.AccelGroup()
        self.add_accel_group(self.accel_group)

        self.command_palette = IdeCommandPalette(application_window=self)

        self.accel_group.connect(Gdk.keyval_from_name('p'),
                                 Gdk.ModifierType.CONTROL_MASK |
                                 Gdk.ModifierType.SHIFT_MASK,
                                 0,
                                 self.command_palette._show)

        # TODO implement a better way to handle paths relative to the IDE root
        builder = Gtk.Builder.new_from_file(os.path.join(
            IdeSettings.root, "layout", "MainWindowHeaderBar.ui"))
        self.header_bar = builder.get_object("app-headerbar")
        self.header_bar.set_title(kwargs['title'])

        builder = Gtk.Builder.new_from_file(os.path.join(
            IdeSettings.root, "layout", "IdeTreeView.ui"))
        builder.connect_signals(Handler())
        self.side_bar = builder.get_object('side-bar-box')
        self.side_bar_stack = builder.get_object('side-bar-stack')
        self.project_revealer = builder.get_object('project-revealer')
        self.open_files_revealer = builder.get_object('open-files-revealer')
        self.open_files_listbox = builder.get_object('open-files-listbox')
        self.tree_view_container = builder.get_object('tree-view-sw')

        self.set_titlebar(self.header_bar)

        self.tree_view = IdeTreeView.IdeTreeView()
        self.tree_view.selected_row.connect("changed",
                                            self.on_tree_file_selected)
        self.tree_view.show()
        self.tree_view_container.add(self.tree_view)

        # Preferences window
        self._build_preferences_window()

        # Editor manager
        self.editor = IdeEditor.IdeEditorManager(self)
        self.editor.connect("editor-changed", self.on_editor_changed)

        # FIXME the layout container can't be a ScrolledWindow
        # cuz some widgets can't be added into them AKA SourceMap
        self.sw = Gtk.ScrolledWindow()
        self.sw.show()

        builder = Gtk.Builder.new_from_file(os.path.join(
            IdeSettings.root, "layout", "EditorNoneOpenViewport.ui"))
        self.sw_none_open_viewport = builder.get_object(
            "editor-none-open-viewport")
        self.sw_none_open_viewport.show()

        self.sw.add(self.sw_none_open_viewport)
        self.bottom_panel = IdeBottomPanel(application_window=self)
        self.bottom_panel.layout.hide()
        # sw.add(self.editor.sview)

        # FIXME the layout CANNOT NOT BE ADDED LIKE THIS! not again...
        self.vpaned = Gtk.Paned(orientation=Gtk.Orientation.VERTICAL)
        self.vpaned.show()

        self.vpaned.add1(self.sw)
        self.vpaned.add2(self.bottom_panel.layout)
        self.vpaned.get_style_context().add_class('bg-base')

        self.paned = Gtk.Paned()
        self.paned.show()
        self.paned.add1(self.side_bar)
        self.paned.add2(self.vpaned)
        self.add(self.paned)

        self.accel_group.connect(Gdk.KEY_quotedbl,
                                 Gdk.ModifierType.CONTROL_MASK,
                                 0,
                                 self._on_bottom_panel_toggle)

        # Preferences window
        action = Gio.SimpleAction.new("preferences", None)
        action.connect("activate", self.on_preferences)
        self.get_application().add_action(action)

        self.plugin_manager = IdePluginManager(application_window=self)

        # self.show_all()

    def _on_bottom_panel_toggle(self, *args):
        if self.bottom_panel.layout.get_visible():
            self.bottom_panel.layout.hide()
        else:
            self.bottom_panel.layout.show()

    def on_tree_file_selected(self, *args):
        """Docstrings for on_tree_file_selected."""
        # TODO handle opening file
        # NOTE remember that some files (e.g. images, videos) shouldn't be
        # handled the same way as text files

        # TODO check if a folder was selected
        if not os.path.isdir(self.tree_view.selected_path):
            self.editor.open(self.tree_view.selected_path)

    def on_editor_changed(self, *args):
        """Docstrings for on_editor_changed."""
        for child in self.sw.get_children():
            self.sw.remove(child)

        # If the user closed all open editors, add the background again
        if (self.editor.active_editor is not None
                and self.editor.active_editor.layout is not None):
            self.sw.add(self.editor.active_editor.layout)
            self.header_bar.set_subtitle(self.editor.active_editor.file)
        else:
            self.header_bar.set_subtitle("")
            self.sw.add(self.sw_none_open_viewport)

        self.sw.show_all()

    def _build_preferences_window(self, *args):

        builder = Gtk.Builder.new_from_file(os.path.join(
            IdeSettings.root, "layout", "PreferencesWindow.ui"))

        self.preferences_window = builder.get_object('preferences_window')
        self.editor_pref_box = builder.get_object('editor_pref_box')
        self.preferences_window.set_transient_for(self)

        self.preferences_window.connect(
            'delete-event', self._on_preferences_quit)

    def on_preferences(self, *args):
        """on_preferences docstrings."""
        self.preferences_window.show()

    def _on_preferences_quit(self, *args):
        self.preferences_window.hide()
        return True
