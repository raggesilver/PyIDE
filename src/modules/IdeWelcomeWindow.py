"""IdeWelcomeWindow module."""
import os
import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk
from modules.IdeSettings import IdeSettings


class IdeWelcomeWindow(Gtk.ApplicationWindow):
    """Docstrings default."""

    def __init__(self, *args):
        """Docstrings deafult."""
        super(IdeWelcomeWindow, self).__init__()
        self.connect('destroy', Gtk.main_quit)

    def do_activate(self, *args):
        """Override."""
        builder = Gtk.Builder.new_from_file(
            os.path.join(IdeSettings.root),
            "layout",
            "WelcomeWindow.ui")

        go = builder.get_object
        self.layout = go('layout')
        self.add(self.layout)
        self.show()
