"""IdeSettings module."""
import os
import sys
import json

from gi.repository import GObject


class _IdeSettings(GObject.GObject):
    """Docstrings for IdeSettings."""

    __gsignals__ = {
        "changed": (
            GObject.SignalFlags.RUN_LAST, None, []
        )
    }

    def __init__(self, path=None, *args):
        """Docstrings for __init__."""
        GObject.GObject.__init__(self)

        self.root = os.path.dirname(
            os.path.abspath(sys.modules['__main__'].__file__))
        self.path = (path if path
                     else os.path.join(self.root, 'pyide-settings.json'))
        self.data = None

        self.get_data()

    def save_data(self, *args):
        with open(self.path, 'w+') as f:
            json.dump(self.data, f, indent=4, sort_keys=True)

    def get_data(self):

        data = None

        try:
            with open(self.path) as f:
                data = json.load(f)

        except Exception:

            data = {
                "ide": {
                    "dark_variant": False
                },
                "ide_editor": {
                    "show_line_numbers": True,
                    "use_spaces_instead_of_tabs": True,
                    "indentation_size": 4,
                    "soft_wrap": True,
                    "show_minimap": True,
                    "auto_indent": True,
                    "smart_home": True,
                    "smart_backspace": True
                }
            }

            with open(self.path, 'w+') as f:
                json.dump(data, f, indent=4, sort_keys=True)

        if data != self.data:
            self.data = data
            self.emit("changed")


IdeSettings = _IdeSettings()
