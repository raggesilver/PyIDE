"""IdeEditor module."""
import gi
import os

from modules.IdeFsManager import IdeFile
from modules.IdeSettings import IdeSettings

gi.require_version('GtkSource', '3.0')

from enum import Enum
from gi.repository import Gtk, Gdk, GtkSource, GObject, Pango


SCREEN_HEIGHT = Gdk.Screen.get_default().height()


class _IdeEditorState(Enum):
    VIEW_MODE = 1
    LOADING_MODE = 2


# TODO save, on_change, set_language
class _IdeEditor(GObject.GObject):
    """Docstrings for _IdeEditor."""

    __gsignals__ = {
        "changed": (  # Text/Content changed
            GObject.SignalFlags.RUN_LAST, None, []
        )
    }

    def __init__(self, _file=None):

        GObject.GObject.__init__(self)

        self.state = _IdeEditorState.LOADING_MODE
        self._last_state = self.state

        self.sview = GtkSource.View()
        self.sbuff = GtkSource.Buffer()
        self.smap = GtkSource.Map()
        self.sgutter = self.sview.get_gutter(Gtk.TextWindowType.LEFT)
        self.language_manager = GtkSource.LanguageManager()
        self.language = None
        self.spinner = Gtk.Spinner()
        self.sviewBox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self.row = None
        self.row_connection_id = None

        self.show_minimap = False

        self.file = _file

        # self.layout = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self.layout = None

        self.do_activate()

    def do_activate(self):
        """Docstrings for activation."""
        self.sview.set_buffer(self.sbuff)
        self.smap.set_view(self.sview)

        self.apply_settings()

        # self.sviewBox.pack_start(self.sview, True, True, 0)
        # self.sviewBox.pack_start(self.smap, False, False, 0)
        # self.sviewBox.show_all()

        # if not self.show_minimap:
        #     self.smap.hide()

        self.spinner.start()
        self.state = _IdeEditorState.LOADING_MODE
        self._set_state()

        # In theory this would be opening a file
        if self.file:
            self.fs_file = IdeFile(path=self.file)
            self.set_text(self.fs_file.read_content())
        # And this creating an empty one
        else:
            # FIXME this might lead to some errors in the future
            self.set_text("")

    def apply_settings(self):
        """Apply the settings to self."""
        # print(IdeSettings.data)
        editor_settings = IdeSettings.data.get("ide_editor")

        if editor_settings is not None:

            self.sview.set_show_line_numbers(
                editor_settings.get("show_line_numbers", False)
            )
            self.sview.set_insert_spaces_instead_of_tabs(
                editor_settings.get("use_spaces_instead_of_tabs", False)
            )
            self.sview.set_indent_width(
                editor_settings.get("indentation_size", 4)
            )
            self.sview.set_wrap_mode(
                (Gtk.WrapMode.WORD_CHAR if editor_settings.get(
                    "soft_wrap", False) else Gtk.WrapMode.NONE)
            )
            self.sview.set_smart_backspace(
                editor_settings.get("smart_backspace", True)
            )
            self.sview.set_smart_home_end(
                editor_settings.get("smart_home", True)
            )
            self.sview.set_indent_on_tab(True)
            self.sview.set_auto_indent(
                editor_settings.get("auto_indent", True)
            )
            self.sview.set_monospace(True)
            # self.sview.set_left_margin(15)
            self.sview.set_buffer(self.sbuff)
            print(self.sview.get_line_yrange(self.sbuff.get_start_iter()))
            self.sview.set_bottom_margin(15 * 80)
            self.show_minimap = editor_settings.get("show_minimap", False)

            self.sgutter_renderer = GtkSource.GutterRendererPixbuf()
            self.sgutter_renderer.set_padding(10, 0)
            self.sgutter.insert(self.sgutter_renderer, 1)

    def set_text(self, text):
        """Docstrings default."""
        if text is not None:

            if self.file:
                self.language = self.language_manager.guess_language(self.file)

            self.sbuff.set_highlight_syntax(self.language is not None)
            self.sbuff.set_language(self.language)
            self.sbuff.begin_not_undoable_action()
            self.sbuff.set_text(text)
            self.sbuff.end_not_undoable_action()
            self.state = _IdeEditorState.VIEW_MODE
            self._set_state()

        # TODO handle else (reading error)

    def _set_state(self):

        # Return if state hasn't changed
        if self.state == self._last_state:
            return

        # Update last state
        self._last_state = self.state

        if self.state == _IdeEditorState.VIEW_MODE:
            self.layout = self.sview
        elif self.state == _IdeEditorState.LOADING_MODE:
            self.layout = self.spinner

        self.layout.show()

    def save(self, *args):
        """Save."""
        if hasattr(self, 'fs_file'):
            rs = self.fs_file.save(self.sbuff)
            print("Save: %s" % rs)
        # Prompt to create a new file
        # This was/is an empty buffer
        else:
            # TODO create a method to handle file creation
            # IDEA the file creation method could be using the command entry
            print('Can\'t save yet')

    def _indent(self, *args):
        print('in')
        # s, e = self.sbuff.get_selection_bounds()
        # self.sview.indent_lines(s, e)

    def _outdent(self, *args):
        print('out')
        # s, e = self.sbuff.get_selection_bounds()
        # self.sview.unindent_lines(s, e)


class IdeEditorManager(GObject.GObject):
    """Docstrings for IdeEditor."""

    __gsignals__ = {
        "editor-changed": (
            GObject.SignalFlags.RUN_LAST, None, []
        ),
        "editor-created": (
            GObject.SignalFlags.RUN_LAST, None, []
        ),
        "editor-created-after": (
            GObject.SignalFlags.RUN_LAST, None, []
        )
    }

    def __init__(self, application_window):
        """Docstrings default."""
        GObject.GObject.__init__(self)

        self.editors = {}
        self.open_files = {}

        self.active_editor = None

        self.application_window = application_window

        self.style_manager = GtkSource.StyleSchemeManager.get_default()
        if os.path.exists(os.path.join(IdeSettings.root, "themes")):
            self.style_manager.append_search_path(
                os.path.join(IdeSettings.root, "themes"))
            print(self.style_manager.get_scheme_ids())

        self.scw = GtkSource.StyleSchemeChooserWidget()
        self.scw.show()
        application_window.editor_pref_box.pack_start(
            self.scw, True, False, 0)

        self.style = IdeSettings.data['ide_editor'].get('style', 'classic')
        self.style = self.style_manager.get_scheme(self.style)
        self.scw.set_style_scheme(self.style)

        self.scw.connect('notify', self._on_style_scheme_changed)
        self._on_style_scheme_changed()

        self.commands = [
            {
                "text": "Save",
                "pre": "Editor",
                "row": None,
                "fn": self.save_current_editor
            },
            {
                "text": "Close",
                "pre": "Editor",
                "row": None,
                "fn": self._delete_current_editor
            }
        ]
        application_window.command_palette.append_commands(self.commands)

        application_window.accel_group.connect(Gdk.keyval_from_name('s'),
                                               Gdk.ModifierType.CONTROL_MASK,
                                               0,
                                               self.save_current_editor)

        application_window.accel_group.connect(Gdk.keyval_from_name('w'),
                                               Gdk.ModifierType.CONTROL_MASK,
                                               0,
                                               self._delete_current_editor)

        application_window.accel_group.connect(Gdk.KEY_bracketleft,
                                               Gdk.ModifierType.CONTROL_MASK,
                                               0,
                                               self.outdent_current_editor)

        application_window.accel_group.connect(Gdk.KEY_bracketright,
                                               Gdk.ModifierType.CONTROL_MASK,
                                               0,
                                               self.indent_current_editor)

    def _on_style_scheme_changed(self, *args):
        self.style = self.scw.get_style_scheme()
        IdeSettings.data['ide_editor']['style'] = self.style.get_id()
        IdeSettings.save_data()
        for editor in self.editors:
            self.editors[editor].sbuff.set_style_scheme(self.style)

    def open(self, path):
        """Docstrings default."""
        created = False

        if path not in self.editors:
            _editor = _IdeEditor(_file=path)
            _editor.sbuff.set_style_scheme(self.style)
            self.editors[path] = _editor
            self.emit("editor-created")
            self._on_new_editor(_editor)
            created = True

        self.active_editor = self.editors[path]

        # Helps plugins better attach to the editors
        if created:
            self.emit("editor-created-after")

        self.emit("editor-changed")

    def save_current_editor(self, *args):
        """Save the current editor."""
        if self.active_editor is not None:
            self.active_editor.save()

    def indent_current_editor(self, *args):
        """Indent the current editor."""
        print('in')
        if self.active_editor is not None:
            self.active_editor._indent()

    def outdent_current_editor(self, *args):
        """Outdent the current editor."""
        print('out')
        if self.active_editor is not None:
            self.active_editor._outdent()

    def delete_editor(self, button, editor):
        """Delete the given editor."""
        # TODO check if the editor is saved first
        if not editor:
            return

        if editor == self.active_editor:
            self.application_window.sw.remove(editor.layout)
            self.active_editor = None
            self.emit("editor-changed")

        if button:
            button.disconnect(self.editors[editor.file].row_connection_id)

        # FIXME there is an error with gsignals when destroying rows
        self.editors[editor.file].row.destroy()
        self.editors.pop(editor.file, None)

    def _delete_current_editor(self, *args):
        self.delete_editor(None, self.active_editor)

    def _on_new_editor(self, editor):
        print('Add editor')

        row = Gtk.ListBoxRow()
        bx = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)

        lbl = Gtk.Label(str(editor.file).replace(
            self.application_window.tree_view.path + os.sep, ""))

        lbl.set_justify(Gtk.Justification.LEFT)
        lbl.set_ellipsize(Pango.EllipsizeMode.START)
        lbl.set_margin_left(5)

        btn = Gtk.Button.new_from_icon_name('window-close-symbolic',
                                            Gtk.IconSize.MENU)
        con = btn.connect('clicked', self.delete_editor, editor)
        btn.get_style_context().add_class('small-btn')
        btn.get_style_context().add_class('flat')

        bx.pack_start(btn, False, False, 5)
        bx.pack_start(lbl, False, False, 0)

        row.add(bx)
        row.show_all()

        self.editors[editor.file].row_connection_id = con
        self.editors[editor.file].row = row
        self.application_window.open_files_listbox.insert(row, 0)
        # self.application_window.open_files_listbox.show_all()

    def _on_delete_editor(self, editor):
        print('Add editor')
