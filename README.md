# PyIDE

![http://opensource.org/licenses/MIT](https://img.shields.io/badge/license-MIT-blue.svg)

This branch is the second rewrite (or 200th maybe?) of PyIDE.


![](preview.png)

## Features
Not many tbh.
- Integrated terminal
- Saves files (after 50 commits lol)
- Plugin support (there are 3 cool plugins available so far)

## Changelog

- Bug fixes
- JSON formatting
- Plugins updated
- [new] Open files display in `IdeTreeView`

---
## Mind thing

### Concept changes

- ~~modules~~ -> plugins 
    - changed due to name conflicts with python (modules and packages)
- code separated in different files (can I hear a Hallelujah?)
- docstrings (no further explanation needed)
- no more hard coded interface (except for "custom" widgets that have their own classes)
- ~~I can use the app-menu now. YEY ^ - ^~~
    - `app-menu`s are (to be) deprecated, moving settings to drop-down menu on the right
- Using `GtkApplication` now, which means that I can provide support for handling `OPEN` and `COMMAND_LINE`
    - ~~Application id is `org.gnome.PyIDE` (hope no one has taken that already)~~
    - Application id is `com.raggesilver.PyIDE`?
- Got rid of ~~`WebKit`~~ cuz that solution was just horrible

### TODO

- Stop being lazy and try once again to implement what Christian Hergert did with his side panels (resizing and hiding + sliding)
- Create a customized widget that stacks `GtkStackSwitcher`'s children (the buttons) in one `GtkComboBox`-like widget for small layouts
    - Application would be in the `side_bar` switcher that currently holds 'Workspace' and the 'Git' plugin (if enabled)

### Not mental notes

**Plugin ideas**


- ~~Git/Gitlab integration (a decent one this time)~~ - Done <span style="color:green">🗸</span>
    - The [git plugin](https://gitlab.com/raggesilver/pyide-git-plugin) has it's own repo and works pretty decently now
- ~~Clone autoBrackets from master~~ Done <span style="color:green">🗸</span>
    - Tho issue open involving unexpected behavior when undoing completed brackets
- Auto completion loader + single auto completion plugins for each language
- Code folding (this one is impossible using `GtkSourceView`, but whatever... one day maybe)
- Correct threaded linting plugin following the same idea as auto completion
- ~~Working compiler, but this time like vscode does (allows user to write what the compiling instructions are rather then just blindingly trying to run a makefile (cuz who does that am I right? :P))~~ - Done <span style="color:green">🗸</span>
    - This idea moved to the [pyide-run-plugin](https://gitlab.com/raggesilver/pyide-run-plugin)
- "Open from where I left" like vscode, atom, firefox


**General ideas**

- ~~Replace `pygit2` with something that works better and is easier to install~~ Done <span style="color:green">🗸</span>
    - Git integration is now a default plugin that can be removed or disabled, and the current implementation uses `gitpython`
- ~~Create a dependency installer (maybe with `npm`?)~~
    - Not using `npm` nor any other package manager alone, instead the `IdePluginManager` will handle the download, installation, removal and dependency checking for every plugin available
- ~~Implement file change watch (using `watchdog`?) to update the `modules.IdeTreeView`~~ Done <span style="color:green">🗸</span>
    - `watchdog` did do the trick :)
- Implement the `Ctrl + Shift + P` overlay every decent modern Ide/Text editor has (would `Gtk.Overlay` do the trick?)

**Gtk 4.0**

- Wait till it comes out to implement these with `Gsk`:
    - Markdown preview
    - HTML/CSS/XML layout preview
